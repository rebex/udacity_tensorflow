from tensorflow.keras.preprocessing.text import Tokenizer

sentences = [
    'My favorite instrument is the piano',
    'I also play the drums',
    'Regan plays the piano',
    'Ruby plays the trumpet and guitar',
    'We like instruments',
    'Do you play an instrument?'
]

# instantiate and fit tokenizer
tokenizer = Tokenizer(num_words=100, oov_token="<OOV>")
tokenizer.fit_on_texts(sentences)

word_index = tokenizer.word_index
print(word_index)
print(word_index['plays'])

# create sequences for the sentences
sequences = tokenizer.texts_to_sequences(sentences)
print(sequences)

# show that OOV occurs when words fit arrive
new_sentences = [
    'The hardest instrument is the organ',
    'Have you tried to play it?',
    'It is crazy hard',
    'You literally have to pull out all of the stops',
    'So both hands, and both feet are working independently!'
]
new_sequences = tokenizer.texts_to_sequences(new_sentences)
print(new_sequences)