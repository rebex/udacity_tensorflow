import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pandas as pd
import tensorflow_datasets as tfds
import numpy as np
import matplotlib.pyplot as plt

# get data
path = tf.keras.utils.get_file('/tmp/sentiment.csv',
                               'https://drive.google.com/uc?id=13ySLC_ue6Umt9RJYSeM2t-V0kCv-4C-P')
dataset = pd.read_csv(path)

sentences = dataset['text'].tolist()
labels = dataset['sentiment'].tolist()

# create subwords dataset
vocab_size = 1000
tokenizer = tfds.deprecated.text.SubwordTextEncoder.build_from_corpus(sentences, vocab_size, max_subword_length=5)

##test tokenizer
# num = 5
# print(sentences[num])
# encoded = tokenizer.encode(sentences[num])
# print(encoded)
#
# for i in encoded:
#     print(tokenizer.decode([i]))

# replace sentence data with encoded subwords
for i, sentence in enumerate(sentences):
    sentences[i] = tokenizer.encode(sentence)

# more preprocessing
max_length = 50
trunc_type = 'post'
padding_type = 'post'

# pad sequences
sequences_padded = pad_sequences(sentences, maxlen=max_length,
                                 padding=padding_type, truncating=trunc_type)

# separate the sentences and labels into training and test
training_size = int(len(sentences)*0.8)

train_sequences = sequences_padded[0:training_size]
test_sequences = sequences_padded[training_size:]
train_labels = labels[0:training_size]
test_labels = labels[training_size:]

# make labels into np arrays
train_labels_final = np.array(train_labels)
test_labels_final = np.array(test_labels)

# create model
embedding_dim = 16

model = tf.keras.Sequential([
    tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=max_length),
    tf.keras.layers.GlobalAveragePooling1D(),
    tf.keras.layers.Dense(6, activation='relu'),
    tf.keras.layers.Dense(1)
])

model.summary()

# train the model
num_epochs = 30
model.compile(loss='binary_crossentropy', optimizer='adam', metrics='accuracy')
history = model.fit(train_sequences, train_labels_final, epochs=num_epochs, validation_data=(test_sequences, test_labels_final))

# plot_graphs returns a graph of the metric listed in 'string' argument
def plot_graphs(history, string):
    plt.plot(history.history[string])
    plt.plot(history.history['val_'+string])
    plt.xlabel('Epochs')
    plt.ylabel(string)
    plt.legend([string, 'val_'+string])
    plt.show()

# plot_graphs(history, 'accuracy')
# plot_graphs(history, 'loss')

# write a function to predict sentiment
def predict_review(model, new_sentences, maxlen=max_length, show_padded_sequences=True):
    new_sequences = []

    # convert the new reviews to sentences
    for i, frvw in enumerate(new_sentences):
        new_sequences.append(tokenizer.encode(frvw))

    # pad all seqeuences for the new reviews
    new_reviews_padded = pad_sequences(new_sequences,maxlen=max_length,padding=padding_type,
                                       truncating=trunc_type)

    classes = model.predict(new_reviews_padded)

    # check positivity of reviews
    for x in range(len(new_sentences)):
        if (show_padded_sequences):
            print(new_reviews_padded[x])
        print(new_sentences[x])
        print(classes[x])
        print('\n')

# Use the model to predict some reviews
fake_reviews = ["I love this phone",
                "Everything was cold",
                "Everything was hot exactly as I wanted",
                "Everything was green",
                "the host seated us immediately",
                "they gave us free chocolate cake",
                "we couldn't hear each other talk because of the shouting in the kitchen"
              ]

predict_review(model, fake_reviews)

def fit_model_now (model, sentences) :
  model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
  model.summary()
  history = model.fit(train_sequences, train_labels_final, epochs=num_epochs,
                      validation_data=(test_sequences, test_labels_final))
  return history

def plot_results (history):
  plot_graphs(history, "accuracy")
  plot_graphs(history, "loss")

def fit_model_and_show_results (model, sentences):
  history = fit_model_now(model, sentences)
  plot_results(history)
  predict_review(model, sentences)

# Define the model
model_bidi_lstm = tf.keras.Sequential([
    tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=max_length),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(embedding_dim)),
    tf.keras.layers.Dense(6, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

# Compile and train the model and then show the predictions for our extra sentences
fit_model_and_show_results(model_bidi_lstm, fake_reviews)

model_multiple_bidi_lstm = tf.keras.Sequential([
    tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=max_length),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(embedding_dim,
                                                       return_sequences=True)),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(embedding_dim)),
    tf.keras.layers.Dense(6, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

fit_model_and_show_results(model_multiple_bidi_lstm, fake_reviews)

my_reviews =["lovely", "dreadful", "stay away",
             "everything was hot exactly as I wanted",
             "everything was not exactly as I wanted",
             "they gave us free chocolate cake",
             "I've never eaten anything so spicy in my life, my throat burned for hours",
             "for a phone that is as expensive as this one I expect it to be much easier to use than this thing is",
             "we left there very full for a low price so I'd say you just can't go wrong at this place",
             "that place does not have quality meals and it isn't a good place to go for dinner",
             ]

print("===================================\n","Embeddings only:\n", "===================================",)
predict_review(model, my_reviews, show_padded_sequences=False)

print("===================================\n", "With a single bidirectional LSTM:\n", "===================================")
predict_review(model_bidi_lstm, my_reviews, show_padded_sequences=False)

print("===================================\n","With two bidirectional LSTMs:\n", "===================================")
predict_review(model_multiple_bidi_lstm, my_reviews, show_padded_sequences=False)