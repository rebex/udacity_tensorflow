from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

sentences = [
    'My favorite instrument is the piano',
    'I also play the drums',
    'Regan plays the piano',
    'Ruby plays the trumpet and guitar',
    'We like instruments',
    'Do you play an instrument?'
]

# instantiate and fit tokeinzer
tokenizer = Tokenizer(num_words=100, oov_token="<OOV>")
tokenizer.fit_on_texts(sentences)

# create sequences
sequences = tokenizer.texts_to_sequences(sentences)
word_index = tokenizer.word_index

# pad sentences
padded = pad_sequences(sequences)
# print('\nWord Index = ', word_index)
# print('\nSequences = ', sequences),
# print('\nPadded Sequences = ',)
# print(padded)

# some padding arguments
# padded = pad_sequences(sequences, maxlen=15)
# print(padded)
# padded = pad_sequences(sequences, maxlen=15, padding='post')
# print(padded)
# padded = pad_sequences(sequences, maxlen=3)
# print(padded)

new_sentences = [
    'The hardest instrument is the organ',
    'Have you tried to play it?',
    'It is crazy hard',
    'You literally have to pull out all of the stops',
    'So both hands, and both feet are working independently!'
]
print(new_sentences)

print('<OOV> has the number ', word_index['<OOV>'], ' in the word index')

test_seq = tokenizer.texts_to_sequences(new_sentences)
print('\nTest Sequence: ', test_seq)
padded = pad_sequences(test_seq, maxlen=10)
print('\nPadded Test Sequence: ')
print(padded)