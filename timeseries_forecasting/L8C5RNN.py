import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

keras = tf.keras

# explanation for code below in other files in the directory
def plot_series(time, series, format="-", start=0, end=None, label=None):
    plt.plot(time[start:end], series[start:end], format, label=label)
    plt.xlabel("Time")
    plt.ylabel("Value")
    if label:
        plt.legend(fontsize=14)
    plt.grid(True)

def trend(time, slope=0):
    return slope * time


def seasonal_pattern(season_time):
    """Just an arbitrary pattern, you can change it if you wish"""
    return np.where(season_time < 0.4,
                    np.cos(season_time * 2 * np.pi),
                    1 / np.exp(3 * season_time))


def seasonality(time, period, amplitude=1, phase=0):
    """Repeats the same pattern at each period"""
    season_time = ((time + phase) % period) / period
    return amplitude * seasonal_pattern(season_time)


def white_noise(time, noise_level=1, seed=None):
    rnd = np.random.RandomState(seed)
    return rnd.randn(len(time)) * noise_level

def model_forecast(model, series, window_size):
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size))
    ds = ds.batch(32).prefetch(1)
    forecast = model.predict(ds)
    return forecast

time = np.arange(4 * 365 + 1)

slope = 0.05
baseline = 10
amplitude = 40
series = baseline + trend(time, slope) + seasonality(time, period=365, amplitude=amplitude)

noise_level = 5
noise = white_noise(time, noise_level, seed=42)

series += noise

plt.figure(figsize=(10, 6))
plot_series(time, series)

def window_dataset(series, window_size, batch_size=32,
                   shuffle_buffer=1000):
    dataset = tf.data.Dataset.from_tensor_slices(series)
    dataset = dataset.window(window_size + 1, shift=1, drop_remainder=True)
    dataset = dataset.flat_map(lambda window: window.batch(window_size + 1))
    dataset = dataset.shuffle(shuffle_buffer)
    dataset = dataset.map(lambda window: (window[:-1], window[-1]))
    dataset = dataset.batch(batch_size).prefetch(1)
    return dataset

# creat train and validation splits
split_time = 1000
time_train = time[:split_time]
x_train = series[:split_time]
time_val = time[split_time:]
x_val = series[split_time:]

# Forecasting using RNN
window_size = 30
train_set = window_dataset(x_train, window_size,batch_size=128)
val_set = window_dataset(x_val, window_size, batch_size=128)
model = keras.models.Sequential([
    keras.layers.Lambda(lambda x: tf.expand_dims(x, axis=-1),
                        input_shape=[None]),
    keras.layers.SimpleRNN(100, return_sequences=True),
    keras.layers.SimpleRNN(100),
    keras.layers.Dense(1),
    keras.layers.Lambda(lambda x: x*200.0)
])
optimizer = keras.optimizers.SGD(lr=1e-5,momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
early_stopping = keras.callbacks.EarlyStopping(patience=50)
model_checkpoint = keras.callbacks.ModelCheckpoint(
    'models/my_checkpoint', save_best_only=True
)
# model.fit(train_set, epochs=500,
#           validation_data=val_set,
#           callbacks=[early_stopping,model_checkpoint])
#
# model = keras.models.load_model('models/my_checkpoint')
# rnn_forecast = model_forecast(
#     model,
#     series[split_time-window_size:-1],
#     window_size)[:,0]
#
# plt.figure(figsize=(10,6))
# plot_series(time_val, x_val)
# plot_series(time_val,rnn_forecast)
#
# mae = keras.metrics.mean_absolute_error(x_val, rnn_forecast).numpy()
# 5.9525437 nope!!

#try again with sequence to sequence
def seq2deq_window_dataset(series, window_size, batch_size=32,
                           shuffle_buffer=1000):
    series = tf.expand_dims(series, axis=-1)
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size + 1, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda window: window.batch(window_size+1))
    ds = ds.shuffle(shuffle_buffer)
    ds = ds.map(lambda window: (window[:-1],window[1:]))
    return ds.batch(batch_size).prefetch(1)

# for X_batch, Y_batch in seq2deq_window_dataset(tf.range(10), 3,
#                                                batch_size=1):
#     print("X: ", X_batch.numpy())
#     print("Y: ", Y_batch.numpy())

# determine LR using LR scheduler
window_size = 30
train_set = seq2deq_window_dataset(x_train, window_size,
                                   batch_size=128)

model = keras.models.Sequential([
    keras.layers.SimpleRNN(100, return_sequences=True,
                           input_shape=[None,1]),
    keras.layers.SimpleRNN(100, return_sequences=True),
    keras.layers.Dense(1),
    keras.layers.Lambda(lambda x: x*200)
])
lr_schedule = keras.callbacks.LearningRateScheduler(
    lambda epoch: 1e-7*10**(epoch/30))
optimizer = keras.optimizers.SGD(lr=1e-7, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
history = model.fit(
    train_set, epochs=100, callbacks=[lr_schedule]
)

plt.semilogx(history.history['lr'], history.history['loss'])
plt.axis([1e-7,1e-4,0,30])

# forecast
val_set = seq2deq_window_dataset(x_val, window_size,
                                 batch_size=128)

optimizer = keras.optimizers.SGD(lr=1e-6, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
early_stopping = keras.callbacks.EarlyStopping(patience=100)
model.fit(train_set, epochs=500,
          validation_data=val_set,
          callbacks=[early_stopping])

rnn_forecast = model_forecast(model, series[..., np.newaxis], window_size)
rnn_forecast = rnn_forecast[split_time - window_size: -1, -1, 0]

plt.figure(figsize=(10,6))
plot_series(time_val, x_val)
plot_series(time_val, rnn_forecast)
plt.show()

mae = keras.metrics.mean_absolute_error(x_val, rnn_forecast).numpy()
print(mae)
# 5.59, not much better than non-sequence to sequence and still doesn't beat MA

