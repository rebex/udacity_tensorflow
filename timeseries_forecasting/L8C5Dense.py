import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

keras = tf.keras


# explanation for code below in other files in the directory
def plot_series(time, series, format="-", start=0, end=None, label=None):
    plt.plot(time[start:end], series[start:end], format, label=label)
    plt.xlabel("Time")
    plt.ylabel("Value")
    if label:
        plt.legend(fontsize=14)
    plt.grid(True)


def trend(time, slope=0):
    return slope * time


def seasonal_pattern(season_time):
    """Just an arbitrary pattern, you can change it if you wish"""
    return np.where(season_time < 0.4,
                    np.cos(season_time * 2 * np.pi),
                    1 / np.exp(3 * season_time))


def seasonality(time, period, amplitude=1, phase=0):
    """Repeats the same pattern at each period"""
    season_time = ((time + phase) % period) / period
    return amplitude * seasonal_pattern(season_time)


def white_noise(time, noise_level=1, seed=None):
    rnd = np.random.RandomState(seed)
    return rnd.randn(len(time)) * noise_level


time = np.arange(4 * 365 + 1)

slope = 0.05
baseline = 10
amplitude = 40
series = baseline + trend(time, slope) + seasonality(time, period=365, amplitude=amplitude)

noise_level = 5
noise = white_noise(time, noise_level, seed=42)

series += noise

plt.figure(figsize=(10, 6))
plot_series(time, series)


def window_dataset(series, window_size, batch_size=32,
                   shuffle_buffer=1000):
    dataset = tf.data.Dataset.from_tensor_slices(series)
    dataset = dataset.window(window_size + 1, shift=1, drop_remainder=True)
    dataset = dataset.flat_map(lambda window: window.batch(window_size + 1))
    dataset = dataset.shuffle(shuffle_buffer)
    dataset = dataset.map(lambda window: (window[:-1], window[-1]))
    dataset = dataset.batch(batch_size).prefetch(1)
    return dataset


# creat train and validation splits
split_time = 1000
time_train = time[:split_time]
x_train = series[:split_time]
time_val = time[split_time:]
x_val = series[split_time:]

# create a linear model
window_size = 30
train_set = window_dataset(x_train, window_size)
val_set = window_dataset(x_val, window_size)

model = keras.models.Sequential([
    keras.layers.Dense(1, input_shape=[window_size])
])
optimizer = keras.optimizers.SGD(lr=1e-5, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
# model.fit(train_set, epochs=100, validation_data=val_set)

# use LR scheduler to find ideal LR
train_set = window_dataset(x_train, window_size)
model = keras.models.Sequential([
    keras.layers.Dense(1, input_shape=[window_size])
])

lr_scheduler = keras.callbacks.LearningRateScheduler(
    lambda epoch: 1e-6 * 10 ** (epoch / 30)
)
optimizer = keras.optimizers.SGD(lr=1e06, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
# history=model.fit(train_set, epochs=100, callbacks=[lr_scheduler])

## plot loss to choose appropriate lr
# plt.semilogx(history.history['lr'],history.history['loss'])
# plt.axis([1e-6,1e-3,0,20])

# use 10^-5=lr
model = keras.models.Sequential([
    keras.layers.Dense(1, input_shape=[window_size])
])

lr = 1e-5
optimizer = keras.optimizers.SGD(lr=lr, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])

# implement early stopping
early_stopping = keras.callbacks.EarlyStopping(patience=10)


# model.fit(train_set, epochs=500,
#           validation_data=val_set,
#           callbacks=[early_stopping])

def model_forecast(model, series, window_size):
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size))
    ds = ds.batch(32).prefetch(1)
    forecast = model.predict(ds)
    return forecast


# lin_forecast = model_forecast(model, series[split_time - window_size:-1], window_size)[:,0]
# lin_forecast.shape
#
# plt.figure(figsize=(10,6))
# plot_series(time_val, x_val)
# plot_series(time_val, lin_forecast)
# plt.show()
#
# mae = keras.metrics.mean_absolute_error(x_val, lin_forecast).numpy()
# print(mae)
# mae = 4.878883, not better than moving average

# add some hidden layers to network to see if the MAE improves
model = keras.models.Sequential([
    keras.layers.Dense(10, activation='relu', input_shape=[window_size]),
    keras.layers.Dense(10, activation='relu'),
    keras.layers.Dense(1)
])

# test LR first
lr = 1e-7
lr_scheduler = keras.callbacks.LearningRateScheduler(
    lambda epoch: lr * 10 ** (epoch / 20)
)
optimizer = keras.optimizers.SGD(lr=lr, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
history = model.fit(train_set, epochs=100, callbacks=[lr_scheduler])

plt.semilogx(history.history['lr'], history.history['loss'])
plt.axis([lr, 5e-3, 0, 30])
plt.show()

# optimal LR = 1e-5
lr = 1e-5

model = keras.models.Sequential([
    keras.layers.Dense(10, activation='relu', input_shape=[window_size]),
    keras.layers.Dense(10, activation='relu'),
    keras.layers.Dense(1)
])

optimizer = keras.optimizers.SGD(lr=lr, momentum=0.9)
model.compile(loss=keras.losses.Huber(),
              optimizer=optimizer,
              metrics=['mae'])
early_stopping = keras.callbacks.EarlyStopping(patience=10)
model.fit(train_set, epochs=500,
          validation_data=val_set,
          callbacks=[early_stopping])

dense_forecast = model_forecast(
    model,
    series[split_time - window_size:-1], window_size)[:, 0]

plt.figure(figsize=(10, 6))
plot_series(time_val, x_val)
plot_series(time_val, dense_forecast)
plt.show()

mae = keras.metrics.mean_absolute_error(x_val, dense_forecast).numpy()
print(mae)
# mae = 5.0578246, still not better than moving average
