import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_datasets as tfds

import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras import layers

import logging
logger = tf.get_logger()
logger.setLevel(logging.ERROR)

# download flowers data set

(training_set, validation_set), info = tfds.load(
    'tf_flowers',
    as_supervised=True,
    split=['train[:80%]','train[80%:]'],
    with_info=True
)

# print info about flowers dataset
num_classes = info.features['label'].num_classes
num_training_examples = int(info.splits['train'].num_examples*0.8)
num_validation_examples = int(info.splits['train'].num_examples*0.2)

print('Total Number of Classes: {}'.format(num_classes))
print('Total Number of Training Images: {}'.format(num_training_examples))
print('Total Number of Validation Images: {} \n'.format(num_validation_examples))

# reformat images
IMAGE_RES = 299

def format_images(image, label):
    image = tf.image.resize(image, (IMAGE_RES,IMAGE_RES))/255
    return image, label

BATCH_SIZE = 32

train_batches = training_set.shuffle(num_training_examples//4).map(format_images).batch(BATCH_SIZE).prefetch(1)
validation_batches = validation_set.map(format_images).batch(BATCH_SIZE).prefetch(1)

# create a feature extractor

URL = 'https://tfhub.dev/google/tf2-preview/inception_v3/feature_vector/4'

feature_extractor = hub.KerasLayer(
    URL,
    input_shape=(IMAGE_RES,IMAGE_RES,3)
)

# freeze pretrained model
feature_extractor.trainable = False

# # add final layer
# model = tf.keras.Sequential([
#     feature_extractor,
#     layers.Dense(num_classes)
# ])
#
# # compile model
# model.compile(
#     optimizer='adam',
#     loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
#     metrics=['accuracy']
# )
#
# # fit model
# EPOCHS = 6
#
# history = model.fit(
#     train_batches,
#     epochs = EPOCHS,
#     validation_data=validation_batches
# )
#
export_path_keras = 'models/inception_model.h5'
#
# model.save(export_path_keras)
# # plot acc/val graphs

model = tf.keras.models.load_model(
  export_path_keras,
  # `custom_objects` tells keras how to load a `hub.KerasLayer`
  custom_objects={'KerasLayer': hub.KerasLayer})

model.summary()

# acc = history.history['accuracy']
# val_acc = history.history['val_accuracy']
#
# loss = history.history['loss']
# val_loss = history.history['val_loss']
#
# epochs_range = range(EPOCHS)
#
# plt.figure(figsize=(8,8))
# plt.subplot(1,2,1)
# plt.plot(epochs_range, acc, label="Training Accuracy")
# plt.plot(epochs_range, val_acc, label='Validation Accuracy')
# plt.title('Accuracy')
# plt.legend(loc='bottom right')
#
# plt.subplot(1,2,2)
# plt.plot(epochs_range, loss, label="Training Loss")
# plt.plot(epochs_range, val_loss, label='Validation Loss')
# plt.title('Loss')
# plt.legend(loc='bottom right')
#
# plt.show()

# try out the model and see what happens!
class_names = np.array(info.features['label'].names)

image_batch, label_batch = next(iter(train_batches.take(1)))

predicted_batch = model.predict(image_batch)
predicted_batch = tf.squeeze(predicted_batch).numpy()

predicted_ids = np.argmax(predicted_batch, axis=-1)
predicted_class_names = class_names[predicted_ids]

plt.figure(figsize=(10,10))
for n in range(30):
    plt.subplot(6,5,n+1)
    plt.subplots_adjust(hspace=0.5)
    plt.imshow(image_batch[n])
    color = 'blue' if predicted_ids[n] == label_batch[n] else 'red'
    plt.title(predicted_class_names[n].title(), color=color)
_ = plt.suptitle('Model Predictions')
plt.show()

