import tensorflow as tf
import numpy as np
import logging
import matplotlib.pyplot as plt

# simple C to F single layer/neuron dense network 

#set up logging
logger = tf.get_logger()
logger.setLevel(logging.ERROR)

# training set
celsius_x = np.array([-40, -10,  0,  8, 15, 22,  38],  dtype=float)
fahrenheit_y = np.array([-40,  14, 32, 46, 59, 72, 100],  dtype=float)

# print training set
for i, c in enumerate(celsius_x):
    print('{} degrees Celsius = {} degrees Fahrenheit'.format(c, fahrenheit_y[i]))

# build model
l0 = tf.keras.layers.Dense(units=1, input_shape=[1])
model = tf.keras.Sequential([l0])

# compile model with loss/optimizer
model.compile(loss='mean_squared_error', 
              optimizer=tf.keras.optimizers.Adam(0.1))

# train model
history = model.fit(celsius_x, fahrenheit_y, epochs=1000, verbose=False) 
print('Finished training')

# plot error
plt.xlabel('Epoch Number')
plt.ylabel('Loss Magnitude')
plt.plot(history.history['loss'])

# predict and show weights
print(model.predict([100.0]))
print("These are the layer variables: {}".format(l0.get_weights()))