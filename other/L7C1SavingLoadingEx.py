import time
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_datasets as tfds
tfds.disable_progress_bar()
from tensorflow.keras import layers

# set up logging
import logging
logger = tf.get_logger()
logger.setLevel(logging.ERROR)

# download dataset
(train_examples, validation_examples), info = tfds.load(
    'tf_flowers',
    split=['train[:80%]','train[80%:]'],
    with_info=True,
    as_supervised=True
)

IMAGE_RES = 224
BATCH_SIZE = 32

# reformat images
def format_image(image, label):
  # `hub` image modules exepct their data normalized to the [0,1] range.
  image = tf.image.resize(image, (IMAGE_RES, IMAGE_RES))/255.0
  return  image, label

num_examples = info.splits['train'].num_examples

BATCH_SIZE = 32
IMAGE_RES = 224

train_batches = train_examples.cache().shuffle(num_examples//4).map(format_image).batch(BATCH_SIZE).prefetch(1)
validation_batches = validation_examples.cache().map(format_image).batch(BATCH_SIZE).prefetch(1)

# implement transfer learning
URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
feature_extractor = hub.KerasLayer(URL,
                                   input_shape=(IMAGE_RES, IMAGE_RES,3))

# freeze existing variables
feature_extractor.trainable = False

# set up model
model = tf.keras.Sequential([
  feature_extractor,
  layers.Dense(5)
])
# model.summary()

# train model
model.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])

EPOCHS = 3
history = model.fit(train_batches,
                    epochs=EPOCHS,
                    validation_data=validation_batches)

# check predictions
class_names = np.array(info.features['label'].names)
print(class_names)

image_batch, label_batch = next(iter(train_batches.take(1)))
image_batch = image_batch.numpy()
label_batch = label_batch.numpy()

predicted_batch = model.predict(image_batch)
# predicted_batch = tf.squeeze(predicted_batch)
# predicted_ids = np.argmax(predicted_batch, axis=-1)
# predicted_class_names = class_names[predicted_ids]
# print(predicted_class_names)
#
# print('Labels: ', label_batch)
# print('Predicted Labels: ', predicted_ids)

# plot predictions
# plt.figure(figsize=(10,9))
# for n in range(30):
#     plt.subplot(6,5,n+1)
#     plt.imshow(image_batch[n])
#     color = 'blue' if predicted_ids[n] == label_batch[n] else 'red'
#     plt.title(predicted_class_names[n].title(), color = color)
#     plt.axis('off')
# _ = plt.suptitle("Model Predictions")

# save model
t = time.time()
#
export_path_keras = "models/{}.h5".format(int(t))
# print(export_path_keras)
#
# model.save(export_path_keras)

reloaded = tf.keras.models.load_model(
    export_path_keras,
    custom_objects={'KerasLayer': hub.KerasLayer}
)

reloaded.summary()

result_batch = model.predict(image_batch)
reloaded_batch = reloaded.predict(image_batch)

# check difference in accuracy
print((abs(result_batch - reloaded_batch)).max())

# keep training!
history = reloaded.fit(train_batches,
                    epochs=EPOCHS,
                    validation_data=validation_batches)

# save a previously saved model
t= time.time()
export_path_sm = "models/{}".format(int(t))
print(export_path_sm)

tf.saved_model.save(model, export_path_sm)


